import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.geom.*;
import java.util.ArrayList;

public class Racing
{
	static ArrayList<Point> points;

	public static void main(String[] args)
	{
		JFrame frame = new JFrame("Tustor racing");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		final PaintPanel app = new PaintPanel(createTrack());
		JScrollPane scroll = new JScrollPane(app);
		frame.getContentPane().add(scroll);

		frame.setSize(400, 400);
		frame.setLocationRelativeTo(null);
		frame.setVisible(true);

		Timer timer=new Timer(50, new ActionListener()
		{
			int index=0;
			@Override
			public void actionPerformed(ActionEvent e)
			{
				index++;
				if (index>=points.size())	index=0;
				app.paint(index);
			}
		});
		timer.start();
	}

	static Shape createTrack()
	{    	
		Path2D.Float track = new Path2D.Float();
		track.moveTo(60, 10);
		track.curveTo(70, 5, 250, 35, 250, 35);
		track.curveTo(400, 100, 250, 130, 250, 200);
		track.curveTo(00, 500, 90, 140, 90, 140);
		track.curveTo(90, 140, 20, 160, 20, 50);
		track.curveTo(10, 30, 3, 10, 60, 10);

		return track;
	}

	static class PaintPanel extends JPanel
	{
		private static final long serialVersionUID = 1L;
		int index;  
		FlatteningPathIterator iter;
		Shape shape;

		public PaintPanel(Shape s)
		{
			this.shape=s;
			iter=new FlatteningPathIterator(s.getPathIterator(new AffineTransform()), 1);
			points=new ArrayList<Point>();
			float[] coords=new float[6];
			while (!iter.isDone())
			{
				iter.currentSegment(coords);
				int x=(int)coords[0];
				int y=(int)coords[1];
				points.add(new Point(x,y));
				iter.next();
			}
		}

		public void paint(int i)
		{
			index = i;
			repaint();
		}

		protected void paintComponent(Graphics g) {
			super.paintComponent(g);
			g.setColor(Color.blue);
			Point p=points.get(index);
			g.fillOval(p.x, p.y, 5,5);
			paintTrack(g, shape);
		}

		protected void paintTrack(Graphics g, Shape shape)
		{
			Graphics2D g2 = (Graphics2D) g;
			g2.setStroke(new CompositeStroke( new BasicStroke( 10f ), new BasicStroke( 0.5f ) ) );
			g.setColor(Color.black);
			g2.draw(shape);
		}
	}

	static class CompositeStroke implements Stroke
	{
		private Stroke stroke1, stroke2;

		public CompositeStroke( Stroke stroke1, Stroke stroke2 )
		{
			this.stroke1 = stroke1;
			this.stroke2 = stroke2;
		}

		public Shape createStrokedShape( Shape shape )
		{
			return stroke2.createStrokedShape( stroke1.createStrokedShape( shape ) );
		}
	}
}