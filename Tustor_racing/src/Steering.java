import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.KeyEventDispatcher;
import java.awt.KeyboardFocusManager;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.geom.AffineTransform;
import java.awt.geom.Path2D;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.Timer;


public class Steering
{
	static final PaintPanel app = new PaintPanel();
	static double x = 150;
	static double y = 340;
	
	static double carHeading = -1.576;
	static double carSpeed;
	static double steerAngle;
	static double underSteer;
	static final double wheelBase = 38;
	
	public static void main(String[] args)
	{
		JFrame frame = new JFrame("Tustor steering");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		KeyboardFocusManager manager = KeyboardFocusManager.getCurrentKeyboardFocusManager();
    manager.addKeyEventDispatcher(new MyDispatcher());

		JScrollPane scroll = new JScrollPane(app);
		frame.getContentPane().add(scroll);

		frame.setSize(400, 400);
		frame.setLocationRelativeTo(null);
		frame.setVisible(true);
		
		Timer timer=new Timer(50, new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent e)
			{
				steer();
				app.repaint();
			}
		});
	
		timer.start();
	}
	
	protected static void steer()
	{
		double angle = steerAngle;
		
		underSteer = (carSpeed * steerAngle);
		if (underSteer>-2 && underSteer<2) underSteer = 0;		
		else
		{
			if (underSteer<0) underSteer = -underSteer;
			underSteer = underSteer / 2;
			angle /= underSteer;
		}
		
		double dt = 1;
		
		double frontX = x + wheelBase/2 * Math.cos(carHeading);
		double frontY = y + wheelBase/2 * Math.sin(carHeading);
			
		double backX = x - wheelBase/2 * Math.cos(carHeading);
		double backY = y - wheelBase/2 * Math.sin(carHeading);
		
		backX += carSpeed * dt * Math.cos(carHeading);
		backY += carSpeed * dt * Math.sin(carHeading);
		frontX += carSpeed * dt * Math.cos(carHeading+angle);
		frontY += carSpeed * dt * Math.sin(carHeading+angle);
		
		x = (frontX + backX) / 2;
		y = (frontY + backY) / 2;
		carHeading = Math.atan2( frontY - backY , frontX - backX );
		
		if (x<0  || x>400 || y<0 || y>400)
		{
		  x = 150;
		  y = 340;
		  carHeading = -1.575;
		}
	}
	
	  
		static class MyDispatcher implements KeyEventDispatcher
    {
      @Override
      public boolean dispatchKeyEvent(KeyEvent e)
      {
      	if (e.getKeyCode()==87) carSpeed+=0.2;
      	if (e.getKeyCode()==65) steerAngle-=0.01;
      	if (e.getKeyCode()==83) steerAngle+=0.01;
      	if (e.getKeyCode()==90) carSpeed-=0.4;
      	
      	if (steerAngle>1) steerAngle = 1;
      	if (steerAngle<-1) steerAngle = -1;
      	if (carSpeed<-2) carSpeed = -2;
 
        return false;
      }
    }

	
	static class PaintPanel extends JPanel
	{
		private static final long serialVersionUID = 1L;
		
		public void paintComponent(Graphics g)
		{
		    super.paintComponent(g);
		    Graphics2D g2d = (Graphics2D)g;
		    
		    g2d.setColor(Color.GREEN);
		    Rectangle out = new Rectangle(0,0,400,400);
		    g2d.draw(out);
		    g2d.fill(out);
		    
		    g2d.setColor(Color.WHITE);
		    Rectangle street = new Rectangle(100,100,300,300);
		    g2d.draw(street);
		    g2d.fill(street);
		    
		    g2d.setColor(Color.GREEN);
		    out = new Rectangle(220,220,180,180);
		    g2d.draw(out);
		    g2d.fill(out);
		    
		    if (x<100 || y<100 || (x>220 && y>200)) g2d.setColor(Color.RED);
		    else  g2d.setColor(Color.BLUE);
		    
		    Rectangle rect = new Rectangle((int)x, (int)y, 40, 20);
		    Path2D.Double path = new Path2D.Double();
		    path.append(rect, false);
		    AffineTransform t = new AffineTransform();
		    t.rotate(carHeading, (int)x+20, (int)y+10);
		    path.transform(t);
		    g2d.draw(path);
		    g2d.fill(path);
		    
		    g2d.setColor(Color.BLACK);
		    g2d.drawString("carSpeed: "+carSpeed+" steerAngle: "+steerAngle,10,10);
		    g2d.drawString("carHeading: "+carHeading, 10, 30);
		    g2d.drawString("underSteering: "+underSteer, 10, 50);
		}
	}
}